package id.bootcamp.configs;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import id.bootcamp.repositories.MSpecializationRepository;
import id.bootcamp.services.MSpecializationService;
import id.bootcamp.services.MSpecializationServiceImpl;

@Configuration
public class IocBeanConfiguration {
	
	@Bean
	MSpecializationService mSpecializationService(MSpecializationRepository specializationRepository) {
		return new MSpecializationServiceImpl(specializationRepository);
	}

}
