package id.bootcamp.controllers;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import id.bootcamp.models.M_Specialization;
import id.bootcamp.services.MSpecializationService;

@RestController
@RequestMapping("api/specialization")
public class MSpecializationRestController {
	
	@Autowired
	private MSpecializationService specializationService;
	
	@GetMapping
	public List<M_Specialization> getAllSpecializations() {
		return specializationService.getAllSpecializations();
	}
	
	@GetMapping("search")
	public List<M_Specialization> searchByName(@RequestParam String search) {
		return specializationService.searchByName(search);
	}
	
	@GetMapping("/{id}")
	public M_Specialization getSpecializationById(@PathVariable("id") Long id) {
		return specializationService.getSpecializationById(id);
	}
	
	@PostMapping()
	public String insertSpecialization(@RequestParam("name") String name) {
		Boolean isExist = specializationService.getAllSpecializations().stream()
				.anyMatch(s -> s.getName().equalsIgnoreCase(name.toLowerCase()));
		
		if (isExist) {
			return "Nama sama";
		}
		
		M_Specialization specialization = new M_Specialization();
		specialization.setName(name);
		specialization.setCreated_by(1l);
		specialization.setCreated_on(new Date());
		specializationService.insertSpecialization(specialization);
		return "ok";
	}
	
	@PutMapping()
	public String editSpecialization(@RequestBody M_Specialization specialization) {
		Boolean isExist = specializationService.getAllSpecializations().stream()
				.anyMatch(s -> s.getName().equalsIgnoreCase(specialization.getName().toLowerCase()));
		
		M_Specialization specializationBefore = specializationService.getSpecializationById(specialization.getId());
		
		if (isExist && !specialization.getName().equalsIgnoreCase(specializationBefore.getName())) {
			return "Nama sama";
		}
		
		
		specialization.setCreated_by(specializationBefore.getCreated_by());
		specialization.setCreated_on(specializationBefore.getCreated_on());
		
		specialization.setModified_by(1l);
		specialization.setModified_on(new Date());
		
		specializationService.editSpecialization(specialization);
		return "ok";
	}
	
	@PutMapping("/{id}")
	public String deleteSpecialization(@PathVariable Long id) {
		M_Specialization specialization = specializationService.getSpecializationById(id);
		
		specialization.setIs_delete(true);
		
		specialization.setDeleted_by(1l);
		specialization.setDeleted_on(new Date());
		
		specializationService.deleteSpecialization(specialization);
		
		return "ok";
	}
	
}
