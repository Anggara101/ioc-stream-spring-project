package id.bootcamp.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping()
public class DefaultController {
	
	@RequestMapping("specialization")
	public String specialization() {
		return "specialization/m_specialization";
	}
	
	@RequestMapping("caridokter")
	public String cariDokter() {
		return "cari_dokter/cari_dokter";
	}
}
