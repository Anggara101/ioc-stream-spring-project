package id.bootcamp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import id.bootcamp.models.M_Specialization;

public interface MSpecializationRepository extends JpaRepository<M_Specialization, Long>{
	
	@Query(value = "SELECT * FROM m_specialization WHERE is_delete = false AND id = :id", nativeQuery = true)
	public M_Specialization getNotDeletedSpecializationById(Long id);
	
}