package id.bootcamp.services;

import java.util.List;

import id.bootcamp.models.M_Specialization;

public interface MSpecializationService {
	
	public List<M_Specialization> getAllSpecializations();
	
	public M_Specialization getSpecializationById(Long id);
	
	public M_Specialization insertSpecialization(M_Specialization specialization);
	
	public M_Specialization editSpecialization(M_Specialization specialization);
	
	public void deleteSpecialization(M_Specialization specialization);
	
	public List<M_Specialization> searchByName(String search);
	
}
