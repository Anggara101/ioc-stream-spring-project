package id.bootcamp.services;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.transaction.annotation.Transactional;

import id.bootcamp.models.M_Specialization;
import id.bootcamp.repositories.MSpecializationRepository;

@Transactional
public class MSpecializationServiceImpl implements MSpecializationService{

	private MSpecializationRepository specializationRepository;
	
	public MSpecializationServiceImpl(MSpecializationRepository specializationRepository) {
		this.specializationRepository = specializationRepository;
	}
	
	@Override
	public List<M_Specialization> getAllSpecializations() {
		return specializationRepository.findAll().stream()
				.filter(s -> s.getIs_delete()==false)
				.sorted((s1, s2) -> s1.getName().compareTo(s2.getName()))
				.collect(Collectors.toList());
	}

	@Override
	public M_Specialization getSpecializationById(Long id) {
		return specializationRepository.getNotDeletedSpecializationById(id);
	}

	@Override
	public M_Specialization insertSpecialization(M_Specialization specialization) {
		return specializationRepository.save(specialization);
	}

	@Override
	public M_Specialization editSpecialization(M_Specialization specialization) {
		return specializationRepository.save(specialization);
	}

	@Override
	public void deleteSpecialization(M_Specialization specialization) {
		specializationRepository.save(specialization);
	}

	@Override
	public List<M_Specialization> searchByName(String search) {
		return getAllSpecializations().stream()
				.filter(s -> s.getName().toLowerCase().contains(search.toLowerCase())).collect(Collectors.toList());
	}

}
