package id.bootcamp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IocStreamProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(IocStreamProjectApplication.class, args);
	}

}
