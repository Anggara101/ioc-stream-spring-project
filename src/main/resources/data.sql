
INSERT INTO public.m_user(
	id, created_by, created_on)
	VALUES 
		( 1, 1, Now());

INSERT INTO public.m_specialization(
	created_by, created_on, name)
	VALUES 
        (1, Now(), 'Spesialis Anak'),
        (1, Now(), 'Spesialis Kulit'),
        (1, Now(), 'Spesialis Gigi');