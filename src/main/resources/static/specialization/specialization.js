var cur_page = 1;
var limit = 5;
var length = limit * (cur_page + 1) + 1;
var total_page = 10;

showAllSpecialization();

// API 

function getAllSpecializationsApi() {
	return $.ajax({
		url: "/api/specialization",
		method: "GET",
		async: false
	});
}

function getSpecializationbyIdApi(id) {
	return $.ajax({
		url: "/api/specialization/" + id,
		method: "GET",
		async: false
	});
}

function getSpecializationByNameApi(name) {
	return $.ajax({
		url: "/api/specialization/search/?search=" + name,
		method: "GET",
		async: false
	});
}

function insertSpecializationApi(name) {
	return $.ajax({
		url: "/api/specialization/?name=" + name,
		method: "POST",
		async: false
	});
}

function editSpecializationApi(objJson) {
	return $.ajax({
		url: "/api/specialization/",
		method: "PUT",
		data: objJson,
		contentType: "application/json",
		async: false
	})
}

function deleteSpecializationApi(id) {
	return $.ajax({
		url: "/api/specialization/" + id,
		method: "PUT",
		async: false
	})
}


function showSpecialization(specializations) {
	$(".table-content").html("");
 	total_page = Math.ceil(specializations.length / limit);
	
	if(cur_page > total_page){
		cur_page = total_page;
	}
	
	length = specializations.length;
	
	$(".pagination").html("");
	
	$(".pagination").append(`
		<li id="prevBtn" class="page-item">
			<a class="page-link" href="#" aria-label="Previous">
			<span aria-hidden="true">&laquo;</span>
			</a>
		</li>
		<li class="page-item page-number active"><a class="page-link" href="#">1</a></li>
	`)
	
	for(i=2; i<=total_page;i++){
		$(".pagination").append(`
			<li class="page-item page-number"><a class="page-link" href="#">${i}</a></li>
		`)
	}
	
	$(".pagination").append(`
		<li id="nextBtn" class="page-item">
			<a class="page-link" href="#" aria-label="Next">
			<span aria-hidden="true">&raquo;</span>
			</a>
		</li>
	`)

	for (i = 0; i < specializations.length; i++) {
		$(".table-content").append(`
			<div class="row table-item justify-content-between">
				<div class="col-6">
					<p>${specializations[i].name}</p>
				</div>
				<div class="col-6 text-end">
					<button value="${specializations[i].id}" class="btn btn-warning edit-btn">
						<i class="bi bi-pencil-square"></i>
					</button>
					<button value="${specializations[i].id}" class="btn btn-danger delete-btn">
						<i class="bi bi-trash"></i>
					</button>
				</div>
			</div>
		`);
	}
	
	pagination(cur_page);
}

function pagination(page){
	cur_page = page;
	$(".pagination li").removeClass("active");
	$(".pagination li").eq(cur_page).addClass("active");
	
	$(".table-content .table-item").hide();
	
	var offset = cur_page * limit - limit
	var curLength = offset + limit;
	if(curLength > length){
		curLength = length;
	}
	
	$(".table-content div.table-item").slice(offset, curLength).show();
}

function showAllSpecialization() {
	var specializations = getAllSpecializationsApi().responseJSON;

	showSpecialization(specializations);

}

function insertSpecialization() {
	var name = $("#nameAddModal").val();
	var response = insertSpecializationApi(name).responseText;
	if (response == "ok"){
		showAllSpecialization();
		$("#addModal").modal("hide");
		$("#nameAddModal").val("");	
	} else{
		alert(response);
	}
	
}

function editSpecialization(id) {
	var name = $("#nameEditModal").val();
	var obj = {
		id: id,
		name: name
	}
	var json = JSON.stringify(obj);

	var response = editSpecializationApi(json).responseText;

	if (response == "ok") {
		$("#editModal").modal("hide");
		showAllSpecialization();
	} else {
		alert(response);
	}
}

function deleteSpecialization(id) {
	var response = deleteSpecializationApi(id).responseText;

	if (response == "ok") {
		$("#deleteModal").modal("hide");
		showAllSpecialization();
	} else {
		alert(response);
	}
}

// handling event

// search
$("#searchForm").submit(function(event) {
	event.preventDefault();
	var search = $("#search").val()
	var specializations = getSpecializationByNameApi(search).responseJSON;

	showSpecialization(specializations);
});

// page number
$(".pagination").on("click", "li.page-number", function(){
	if(cur_page == $(this).index()){
		return;
	}
	pagination($(this).index());
});

// prev page
$(".pagination").on("click", "#prevBtn", function(){
	if(cur_page == 1){
		return;
	}
	cur_page--;
	pagination(cur_page);
});

// next page
$(".pagination").on("click", "#nextBtn", function(){
	if(cur_page == total_page){
		return;
	}
	cur_page++;
	pagination(cur_page);
});

// add button
$("#addBtn").click(function() {
	$("#addModal").modal("show");
});

// add button in modal
$("#addForm").submit(function(event) {
	event.preventDefault();
	insertSpecialization()
});

// edit button
$("body").on("click", ".edit-btn", function() {
	var id = $(this).attr("value");
	$("#editModal").modal("show");
	$("#editId").val(id);
	var specialization = getSpecializationbyIdApi(id).responseJSON;
	$("#nameEditModal").val(specialization.name);

})

// edit button in modal
$("#editForm").submit(function(event) {
	event.preventDefault();
	var id = $("#editId").val();
	editSpecialization(id);
});

// delete button
$("body").on("click", ".delete-btn", function() {
	var id = $(this).attr("value");
	$("#deleteModal").modal("show");
	$("#deleteId").val(id);
	var specialization = getSpecializationbyIdApi(id).responseJSON;
	$("#nameDeleteModal").text("Anda akan menghapus spesialisasi " + specialization.name + "?");
})

// delete button in modal
$("#deleteForm").submit(function(event) {
	event.preventDefault();
	var id = $("#deleteId").val();
	deleteSpecialization(id);
});